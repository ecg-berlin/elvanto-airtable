require 'yaml'
require 'airrecord'

# see https://github.com/sirupsen/airrecord

class Service < Airrecord::Table

    def set_default_unsaved (positions)
        positions.each do |position|
            set_person_unsaved position
        end
    end

    def set_person_unsaved(position)
        return if self[position].nil? or self[position].strip.empty?
        if self[position + ' Elvanto'].nil? or self[position + ' Elvanto'] == false
            self[position + ' Elvanto'] = false
        end
    end

    def set_person(position, array, key) 
        # this array is from elvanto. If there key exists, then we have a persons assigned to this position:
        if array.key? key
            self[position] = array[key].first # select the first name from elvanto
            if self[position + ' Elvanto'].nil? or self[position + ' Elvanto'] != true
                self[position + ' Elvanto'] = true
            end
        else # we don't have any data:
            if  not self[position].nil? and not self[position].empty?
                set_person_unsaved position 
            end
        end
    end

    def get_config
        $config['services'].each do |key, service_config|
            if service_config['name'] == self['Typ']
                return service_config
            end
        end
        return nil
    end
end

def get_next_services(count)
    a = Class.new(Service) do |klass|
        klass.table_name = $config['airtable_table_name']
        klass.api_key = $config['airtable_api_key']
        klass.base_key = $config['airtable_base_key']
    end
    a.all(
        filter: "DATETIME_DIFF({Datum}, TODAY(), 'days') >= 0 ",
        sort: {"Datum" => "asc"}
    ).first(count)
end

