require 'yaml'
require 'faraday'
require 'date'
class ElvantoServices
    # Returns a basic connection to get information about services
    # See https://www.elvanto.com/api/services/getAll/
    def get_service_conn()
        conn = Faraday.new(
            url: 'https://api.elvanto.com/v1/services/getAll.json',
            headers: {'Content-Type' => 'application/json'}
        )
        conn.basic_auth($config['elvanto_api_key'], 'x')
        conn
    end

    # Returns all Services on a certain date
    def get_services(date)
        pp date
        startDate = Date.strptime(date, '%Y-%m-%d')
        endDate = startDate.next_day
        conn = get_service_conn
        resp = conn.post() do |req|
            req.body = {
                start: startDate.to_s, 
                end: endDate.to_s, 
                fields: [
                    "series_name",
                    "service_times",
                    "plans",
                    "volunteers",
                ]    
            }.to_json
        end
        JSON.parse(resp.body)
    end

    def get_service(date, category)
        services = get_services(date)
        return nil if services['services'].nil?
        service = services['services']['service'].find { |s| s['service_type']['id'] == category}
    end

    def get_positions(eService)
        ret = {}
        return nil unless eService.key? 'volunteers'
        return nil if eService["volunteers"].empty?
        plan = eService["volunteers"]["plan"].first
        plan["positions"]["position"].each do |pos|
            next if pos["volunteers"] === ""
            ret[pos['department_name']] = {} unless ret.key? pos['department_name']
            volunteers = pos['volunteers']['volunteer'].map {|x| x['person']['firstname'] + ' ' + x['person']['lastname']}
            ret[pos['department_name']][pos['position_name']] = volunteers
        end
        ret
    end

end