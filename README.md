# Elvanto Airtable 

## Why?

Um eine Übersicht zu haben, über Mitarbeiter und die Planung zu erleichtern. Tabellen sind einfacher für Planer, Elvanto ist besser für Mitarbeiter.

## Install

```bash
sudo gem install airrecord
```
