require 'pp'
require 'time'
require 'optparse'

require_relative 'airtable'
require_relative 'elvanto'

def main()

    # default config option
    options = {
        :config => './services.yml',
        :service_count => 20,
    }

    #read config from console
    OptionParser.new do |opts|
        opts.banner = "Usage: sync.rb [options]"
        opts.on("-v", "--verbose", "Show extra information") do
          options[:verbose] = true
        end
        opts.on("-c path", "--config path", "Path to config file") do |path|
          options[:config] = path
        end

        opts.on("--service-count number", "How many services to check") do |number|
            options[:service_count] = number
          end
    end.parse!
    pp options
    $config = YAML.load_file(options[:config])
    Airrecord.api_key = $config['airtable_api_key']
        
    elvanto = ElvantoServices.new
    services = get_next_services(options[:service_count])
    services.each do |aService|
        oldService = YAML::dump(aService)
        next if aService['Datum'].nil? or aService['Typ'].nil?
        next if aService['Datum'].empty? or aService['Typ'].empty?

        config = aService.get_config

        eService = elvanto.get_service(aService['Datum'], config['elvanto_category'])
        if eService.nil?
            aService.set_default_unsaved $config['checkable_positions']
        else
            positions = elvanto.get_positions(eService)
            if positions.nil? 
                aService.set_default_unsaved $config['checkable_positions']
            else
                set_new_values(aService, positions, config['positions'])
            end
        end
        if oldService != YAML::dump(aService)
            puts YAML::dump(aService)
            begin
                aService.save
            rescue => exception
                pp exception
            end
            
        end
    end
end


def set_new_values(airtable_service, positions, config)
    positions.each do |department, positions|
        if config.key? department
            config[department].each do |airtable_name, elvanto_name|
                airtable_service.set_person airtable_name, positions, elvanto_name
            end
        end
    end
end

main()